// Transform.cpp
// OpenGL SuperBible
// Demonstrates manual transformations
// Program by Richard S. Wright Jr.

#include <gltools.h>	// OpenGL toolkit
#include <math3d.h>    // 3D Math Library
#include <math.h>
#include <stdio.h>


void drawColor();
void DrawBall(M3DMatrix44f mTransform)
{
    GLfloat radius = .55f;
    GLint segment = 30;

    int i, j;
    M3DVector3f objectVertex;
    M3DVector3f transformedVertex;
    glPushMatrix();
    for(i = 0; i < segment; ++i){
        //caculate radius
        GLfloat currentradius1 = ((GLfloat)sin(M3D_PI  / segment * i)) * radius ;
        GLfloat currentradius2 = ((GLfloat)sin(M3D_PI  / segment * (i + 1) )) * radius;
        GLfloat y1 = cos(M3D_PI / segment * i) * radius;
        GLfloat y2 = cos(M3D_PI / segment * (i + 1 )) * radius;
//        glRotatef(45, 1.0f, 1.0f, 1.0f);
        GLfloat dz = 0;
        glColor3f(1.0f, 0.0f, 1.0f);
        glBegin(GL_TRIANGLE_STRIP);
        for(j =0 ; j <= segment ; ++j){
             GLfloat ang = M3D_PI * 2.0f  / segment * j;
             GLfloat x1 = cos(ang) * currentradius1;
             GLfloat z1 = sin(ang) * currentradius1;
             objectVertex[0] = x1;
             objectVertex[1] = y1;
             objectVertex[2] = z1 + dz;
            m3dTransformVector3(transformedVertex, objectVertex, mTransform);
         drawColor();
            glVertex3fv(transformedVertex);
//             glVertex3fv(objectVertex);

             GLfloat x2 = cos(ang) * currentradius2;
             GLfloat z2 = sin(ang) * currentradius2;
             objectVertex[0] = x2;
             objectVertex[1] = y2;
             objectVertex[2] = z2 + dz;
            m3dTransformVector3(transformedVertex, objectVertex, mTransform);
         drawColor();
            glVertex3fv(transformedVertex);
            //glVertex3fv(objectVertex);

            // printf("x1 %f y1 %f z1 %f \r\nx2 %f y2 %f z2 %f\r\n\r\n", x1, y1, z1, x2, y2, z2);

        }
        glEnd();
        glPopMatrix();
    }
}

// Draw a torus (doughnut), using the current 1D texture for light shading
void DrawTorus(M3DMatrix44f mTransform)
    {
    GLfloat majorRadius = 0.35f;
    GLfloat minorRadius = 0.15f;
    GLint   numMajor = 40;
    GLint   numMinor = 20;
    M3DVector3f objectVertex;         // Vertex in object/eye space
    M3DVector3f transformedVertex;    // New Transformed vertex   
    double majorStep = 2.0f*M3D_PI / numMajor;
    double minorStep = 2.0f*M3D_PI / numMinor;
    int i, j;
    for (i=0; i<numMajor; ++i) 
        {
        double a0 = i * majorStep;
        double a1 = a0 + majorStep;
        GLfloat x0 = (GLfloat) cos(a0);
        GLfloat y0 = (GLfloat) sin(a0);
        GLfloat x1 = (GLfloat) cos(a1);
        GLfloat y1 = (GLfloat) sin(a1);


        //glColor3f(1.0f, 0.0f, 0.0f);
//        glPushMatrix();
        glBegin(GL_TRIANGLE_STRIP);
        for (j=0; j<=numMinor; ++j) 
            {
            double b = j * minorStep;
            GLfloat c = (GLfloat) cos(b);
            GLfloat r = minorRadius * c + majorRadius;
            GLfloat z = minorRadius * (GLfloat) sin(b);

            // First point
            objectVertex[0] = x0*r;
            objectVertex[1] = y0*r;
            objectVertex[2] = z;
            m3dTransformVector3(transformedVertex, objectVertex, mTransform);
            drawColor();
            glVertex3fv(transformedVertex);

            // Second point
            objectVertex[0] = x1*r;
            objectVertex[1] = y1*r;
            objectVertex[2] = z;
            m3dTransformVector3(transformedVertex, objectVertex, mTransform);
            drawColor();
            glVertex3fv(transformedVertex);
            }
        glEnd();
//        glPopMatrix();
        }
    }
        
void drawColor()
{
    //return;
            glColor3ub((GLubyte)255, (GLubyte)0, (GLubyte)0);
            return;
    static int i = 0;
    switch(i % 3){
        case 0:
            glColor3ub((GLubyte)255, (GLubyte)0, (GLubyte)0);
            break;
        case 1:
            glColor3ub((GLubyte)0, (GLubyte)255, (GLubyte)0);
            break;
        case 2:
            glColor3ub((GLubyte)0, (GLubyte)0, (GLubyte)255);
            break;
    }
    i++;
}

// Called to draw scene
void RenderScene(void)
    {
    M3DMatrix44f   transformationMatrix;   // Storeage for rotation matrix
    static GLfloat yRot = 0.0f;         // Rotation angle for animation
    yRot += 0.5f;
        
    // Clear the window with current clearing color
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
    // Build a rotation matrix
    m3dRotationMatrix44(transformationMatrix, m3dDegToRad(yRot), 0.0f, 1.0f, 0.0f);
    transformationMatrix[12] = 0.0f;
    transformationMatrix[13] = 0.0f;
    transformationMatrix[14] = -2.9f;
        
//    DrawTorus(transformationMatrix);
    DrawBall(transformationMatrix);

    // Do the buffer Swap
    glutSwapBuffers();
    }

// This function does any needed initialization on the rendering
// context. 
void SetupRC()
    {
    // Bluish background
    glClearColor(0.0f, 0.0f, .50f, 1.0f );
         
    // Draw everything as wire frame
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }

///////////////////////////////////////////////////////////
// Called by GLUT library when idle (window not being
// resized or moved)
void TimerFunction(int value)
    {
    // Redraw the scene with new coordinates
    glutPostRedisplay();
    glutTimerFunc(33,TimerFunction, 1);
    }



void ChangeSize(int w, int h)
    {
    GLfloat fAspect;

    // Prevent a divide by zero, when window is too short
    // (you cant make a window of zero width).
    if(h == 0)
        h = 1;

    glViewport(0, 0, w, h);

 /*   GLfloat ambientLight[] = {1.0f, 1.0f, 1.0f, 1.0f};
    glEnable(GL_LIGHTING);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
   */     
    fAspect = (GLfloat)w / (GLfloat)h;

    // Reset the coordinate system before modifying
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	
    // Set the clipping volume
    gluPerspective(35.0f, fAspect, 1.0f, 50.0f);
        
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    }

int main(int argc, char* argv[])
    {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(800,600);
    glutCreateWindow("Manual Transformations Demo");
    glutReshapeFunc(ChangeSize);
    glutDisplayFunc(RenderScene);

    SetupRC();
    glutTimerFunc(33, TimerFunction, 1);

    glutMainLoop();

    return 0;
    }
