#include <gltools.h>
#include "math3d.h"

static GLfloat xRot = 0.0f;
static GLfloat yRot = 0.0f;

void RenderScene(void)
{
	M3DVector3f vNormal;

//    glMatrixMode(GL_PROJECTION);
//    glLoadIdentity();
//    gluPerspective(90.0f, 8.0f / 6.0f, 1.0f, 201.0f);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	{
	glPushMatrix();
	glRotatef(xRot, 1.0f, 0.0f, 0.0f);
	glRotatef(yRot, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);
	glColor3ub(100, 100, 100);

	M3DVector3f p1 = {-25.0f, 25.0f, 0};
	M3DVector3f p2 = {-25.0f, -25.0f, 0};
	M3DVector3f p3 = {25.0f, -25.0f, 0};
	M3DVector3f p4 = {25.0f, 25.0f, 0};


	m3dFindNormal(vNormal, 	p1, p2, p3);
	glNormal3fv(vNormal);

	glVertex3fv(p1);
	glVertex3fv(p3);
	glVertex3fv(p2);

	m3dFindNormal(vNormal, p1, p4, p3);
	glNormal3fv(vNormal);

	glVertex3fv(p1);
	glVertex3fv(p4);
	glVertex3fv(p3);

	glEnd();
	glPopMatrix();
	}

	{
	glPushMatrix();
	glTranslatef(50.0f, 50.0f, 50.0f);
	glRotatef(xRot, 1.0f, 0.0f, 0.0f);
	glRotatef(yRot, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);
	glColor3ub(100, 100, 100);

	M3DVector3f p1 = {-25.0f, 25.0f, 0};
	M3DVector3f p2 = {-25.0f, -25.0f, 0};
	M3DVector3f p3 = {25.0f, -25.0f, 0};
	M3DVector3f p4 = {25.0f, 25.0f, 0};


	m3dFindNormal(vNormal, 	p1, p2, p3);
	glNormal3fv(vNormal);

	glVertex3fv(p1);
	glVertex3fv(p3);
	glVertex3fv(p2);

	m3dFindNormal(vNormal, p1, p4, p3);
	glNormal3fv(vNormal);

	glVertex3fv(p1);
	glVertex3fv(p4);
	glVertex3fv(p3);

	glEnd();
	glPopMatrix();
	}

	glutSwapBuffers();
	}

void SetupRC()
{
//	GLfloat ambientLight[] = {1.0f, 1.0f, 1.0f, 1.0f};
	glEnable(GL_DEPTH_TEST);
	glFrontFace(GL_CCW);

	glEnable(GL_LIGHTING);


	GLfloat ambientLight[] = {.3f, .3f, .3f, 1.0f};
	GLfloat diffuseLight[] = {.7f, .7f, .7f, 1.0f};
	GLfloat specular[] = {1.0f, 1.0f, 1.0f, 1.0f};
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);

	glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
	glEnable(GL_LIGHT0);

	glEnable(GL_COLOR_MATERIAL);

	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
//	glColorMaterial(GL_BACK, GL_AMBIENT_AND_DIFFUSE);

	GLfloat specref[] = {1.0f, 1.0f, 1.0f, 1.0f};

	glMaterialfv(GL_FRONT, GL_SPECULAR, specref);
	glMateriali(GL_FRONT, GL_SHININESS, 128);
	glMaterialfv(GL_BACK, GL_SPECULAR, specref);
	glMateriali(GL_BACK, GL_SHININESS, 128);

	glClearColor(0.0f, 0.0f, .5f, 1.0f);
	glEnable(GL_NORMALIZE);
 	}

void SpecialKeys(int key, int x, int y)
{
	if(key == GLUT_KEY_UP)
		xRot -= 5.0f;

	if(key == GLUT_KEY_DOWN)
		xRot += 5.0f;

	if(key == GLUT_KEY_LEFT)
		yRot -= 5.0f;

	if(key == GLUT_KEY_RIGHT)
		yRot += 5.0f;

	glutPostRedisplay();
	}

void ChangeSize(int w, int h)
{
    GLfloat fAspect;
    GLfloat lightPos[] = { -50.f, 50.0f, 1000.0f, 1.0f };

    // Prevent a divide by zero
    if(h == 0)
        h = 1;

    // Set Viewport to window dimensions
    glViewport(0, 0, w, h);

    // Reset coordinate system
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    fAspect = (GLfloat) w / (GLfloat) h;
    gluPerspective(90.0f, fAspect, 1.0f, 225.0f);

//    gluLookAt(100.0f, 0.0f, 50.0f, 25.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glLightfv(GL_LIGHT0,GL_POSITION,lightPos);
    glTranslatef(0.0f, 0.0f, -150.0f);

	}

int main(int argc, char* argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Triangle");
	glutReshapeFunc(ChangeSize);
	glutSpecialFunc(SpecialKeys);
	glutDisplayFunc(RenderScene);
	SetupRC();
	glutMainLoop();

	return 0;

	}
