#include "../lib/gltools.h"
#include <math.h>

#define GL_PI 3.1415f

static GLfloat xRot = 0.0f;
static GLfloat yRot = 0.0f;

void RenderScene(void)
{
    GLfloat x, y, z, angle;
    glClear(GL_COLOR_BUFFER_BIT);

    glPushMatrix();
    glRotatef(xRot, 1.0f, 0.0f, 0.0f);
    glRotatef(yRot, 0.0f, 1.0f, 0.0f);

    glBegin(GL_POINTS);
    {
        z = -50.0f;
        //螺旋上升圆
        for(angle = 0.0f; angle <= (2.0f * GL_PI)*3.0f; angle += 0.1f){
            x = 50.0f * sin(angle);
            y = 50.0f * cos(angle);

            //glVertex3f(x, y, z);
            z += 0.5f;
        }
       //正弦波
        for(GLfloat x1 = -100.0f; x1 < 100.0f; x1 += 0.01f){
            GLfloat y1 = sin(x1 / 2.0f ) * 10.0f;
            //glVertex3f(x1, y1, 0);
        }
        //螺旋放大的圆
        GLfloat r = 0;
        for(GLfloat ang = 0.0f; ang < (10.0f * GL_PI * 2); ang += 0.1f, r += 0.2f)
        {
            GLfloat x2 = r * cos(ang);
            GLfloat y2 = r * sin(ang);
            //glVertex3f(x2, y2, r / 2.0f);
        }
    }
    glEnd();
    //射线
    glBegin(GL_LINES);
    {
        GLfloat r = 100.0f;
        for(int i = 0; i < 20; i++){
            GLfloat x1 = cos(GL_PI / 20 * i) * r;
            GLfloat y1 = sin(GL_PI / 20 * i) * r;
            glVertex3f(x1, y1, 0);
            glVertex3f(-x1, -y1, 0);

        }
    }
    glEnd();

    glPopMatrix();
    glutSwapBuffers();
}

void SetupRC()
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glColor3f(0.0f, 1.0f, 0.0f);
}

void SpecialKeys(int key, int x, int y)
{
    if(key == GLUT_KEY_UP)
        xRot -= 5.0f;
    if(key == GLUT_KEY_DOWN)
        xRot += 5.0f;
    if(key == GLUT_KEY_LEFT)
        yRot -= 5.0f;
    if(key == GLUT_KEY_RIGHT)
        yRot += 5.0f;
    if(key > 356.0f)
        xRot = 0.0f;
    if(key < -1.0f)
        xRot = 355.0f;
    if(key > 356.0f)
        yRot = 0.0f;
    if(key < -1.0f)
        yRot = 355.0f;
    glutPostRedisplay();
}

void ChangeSize(int w, int h)
{
    GLfloat nRange = 100.0f;
    if(h == 0)
        h = 1;

    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    if(w <= h)
        glOrtho( -nRange, nRange, -nRange * h / w, nRange * h / w, -nRange, nRange);
    else
        glOrtho( -nRange * w / h, nRange * w / h, -nRange, nRange, -nRange, nRange);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

int main(int argc, char* argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutCreateWindow("Points Example");
    glutReshapeFunc(ChangeSize);
    glutSpecialFunc(SpecialKeys);
    glutDisplayFunc(RenderScene);
    SetupRC();
    glutMainLoop();
}
